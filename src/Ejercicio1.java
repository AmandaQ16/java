import java.util.*;
import javax.sound.sampled.SourceDataLine;

/*Crear un programa que solicite el tamaño de un array
ingrese los datos y calcule el promedio de los datos pares 
e impares*/

public class Ejercicio1 {
    public static void main(String args[]) {
      Scanner r = new Scanner(System.in);//escaner númerico
      int tam, cantPares=0, cantImpares=0;
      System.out.println("");
      System.out.println("Digite el tamaño del Array");
      tam=r.nextInt();
      
      int Numeros [] = new int [tam];
      double acumPares=0, acumImpares=0, promedioPares=0, promedioImpares=0;

    for(int i=0;i<tam;i++){
        System.out.println("");
        System.out.println("Ingrese un numero:");
        Numeros[i]=r.nextInt();
        
        if(Numeros[i]%2==0){
            cantPares++;
        } else{
            cantImpares++;
        }
    }
    
    int Pares[] = new int[cantPares]; 
    int Impares[] = new int[cantImpares];

    cantPares = 0;
    cantImpares = 0;

    for(int i=0; i<tam; i++){
        if (Numeros[i]%2==0) {
            Pares[cantPares] = Numeros[i];
            cantPares++;
        }

        if (Numeros[i]%2!=0) {
            Impares[cantImpares] = Numeros[i];
            cantImpares++;
        }
    }

    for(int i=0; i<cantPares; i++){
        acumPares += Pares[i];
    }

    for(int i=0; i<cantImpares; i++){
        acumImpares += Impares[i];
    }


    promedioPares = acumPares/cantPares;
    promedioImpares = acumImpares/cantImpares;


    System.out.println("");
    System.out.println("-----------------------------");
    System.out.println("Hay "+ cantPares+ " numeros pares");
    System.out.println("Hay "+ cantImpares+ " numeros impares");
    System.out.println("-----------------------------");
    System.out.println("ARREGLO PAR:");
    System.out.println(Arrays.toString(Pares));
    System.out.println("El promedio es: " + promedioPares);
    System.out.println("-----------------------------");
    System.out.println("ARREGLO IMPAR:");
    System.out.println(Arrays.toString(Impares));
    System.out.println("El promedio es: " + promedioImpares);
    }
}