import java.util.*;
import javax.sound.sampled.SourceDataLine;

/*Crear un programa que solicite el tamaño de un array
ingrese los datos y calcule el dato de la mitad del array 
para tamaños pares e impares*/

public class Ejercicio2 {
    public static void main(String args[]) {
      Scanner r = new Scanner(System.in);//escaner númerico
      int tam, mitad;
      System.out.println("");
      System.out.println("Digite el tamaño del Array");
      tam=r.nextInt();
      
      int Numeros [] = new int [tam];

    for(int i=0;i<tam;i++){
        System.out.println("");
        System.out.println("Ingrese un numero:");
        Numeros[i]=r.nextInt();
    }

    mitad=tam/2;
    System.out.println(Arrays.toString(Numeros));

    if (tam%2==0) {
        System.out.println("Un dato de la mitad es: " + Numeros[mitad]);
        System.out.println("Un dato de la mitad es: " + Numeros[mitad-1]);
    } else{
        System.out.println("El dato de la mitad es: " + Numeros[mitad]);
    }

    }
}